import { type } from '@testing-library/user-event/dist/type';
import './App.css';
import Persons from './Components/Persons';

function App() {
  return (
    <div className="App">
      <Persons />
    </div>
  );
}

export default App;
