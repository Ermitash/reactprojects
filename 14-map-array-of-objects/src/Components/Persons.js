import data from '../Data/PersonData.json';
import Person from './Person';

function Persons() {
  return (
    <div className="cards">
      {data.map((value) => {
        return <Person key={value.id} {...value} />;
      })}
    </div>
  );
}

export default Persons;
