const App = ({ intitialButtonText, initialClassesList }) => {
  const [buttonText, setButtonText] = React.useState(intitialButtonText);
  const [ClassesList, setClassesList] = React.useState(initialClassesList);

  const onButtonClick = () => {
    setButtonText('Hello from React');
    setClassesList('green-btn');
  };

  return (
    <div className="app">
      <button className={ClassesList} onClick={onButtonClick}>
        {buttonText}
      </button>
    </div>
  );
};

const container = document.getElementById('app');
const root = ReactDOM.createRoot(container);
root.render(<App intitialButtonText="Click me" initialClassesList="" />);
