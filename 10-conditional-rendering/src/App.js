import './App.css';
import PetInfo from './Components/PetInfo';

function App() {
  return (
    <div className="App">
      <PetInfo animal="cat" age="4" hasPet={true} />
      <PetInfo animal="cat" age="4" hasPet={false} />
    </div>
  );
}

export default App;
