import TodoForm from './Components/Todos/TodoForm';
import './App.css';
import { TodoList } from './Components/Todos/TodoList';

function App() {
  return (
    <div className="App">
      <h1>Todo App</h1>
      <TodoForm />
      <TodoList />
    </div>
  );
}

export default App;
