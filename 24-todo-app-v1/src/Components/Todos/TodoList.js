import styles from './TodoList.module.css';
import Todo from './Todo';
import { useState } from 'react';

var setTodoList_External = null;
var removeElementFromTodoList_External = null;

function TodoList() {
  function removeElementFromTodoList(index) {
    setTodoList((oldArray) => {
      oldArray.splice(index, 1);
      return [...oldArray];
    });
  }

  const [todoList, setTodoList] = useState([]);
  setTodoList_External = setTodoList;
  removeElementFromTodoList_External = removeElementFromTodoList;

  return todoList.length === 0 ? (
    <h1>Empty todo!</h1>
  ) : (
    <div className={styles.TodoList}>
      {todoList.map((value, index) => {
        return <Todo key={index} id={index} text={value} />;
      })}
    </div>
  );
}

export { setTodoList_External, removeElementFromTodoList_External, TodoList };
