import { removeElementFromTodoList_External } from './TodoList';
import styles from './Todo.module.css';
import journalIcon from '../../Img/journal-text.png';

function Todo({ text, id }) {
  return (
    <div
      onClick={() => {
        removeElementFromTodoList_External(id);
      }}
      className={styles.Todo}
    >
      <img className={styles.journalIcon} src={journalIcon} alt="icon" />
      <h2 className={styles.Todo_text}>{text}</h2>
    </div>
  );
}

export default Todo;
