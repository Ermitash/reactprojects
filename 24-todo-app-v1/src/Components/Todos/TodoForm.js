import React, { useState } from 'react';
import Button from '../UI/Button';
import { setTodoList_External } from './TodoList';
import styles from './TodoForm.module.css';

function TodoForm() {
  const [inputData, setInputData] = useState('');

  const onFromSubmit = (event) => {
    event.preventDefault();

    if (inputData != '') {
      setTodoList_External((oldArray) => [...oldArray, inputData]);
      setInputData('');
    }
  };

  return (
    <form className={styles.TodoForm} onSubmit={onFromSubmit}>
      <input
        className={styles.TodoForm_input}
        type="text"
        value={inputData}
        onChange={(e) => {
          setInputData(e.target.value);
        }}
        placeholder="Enter new todo"
      />
      <Button type="submit" />
    </form>
  );
}

export default TodoForm;
