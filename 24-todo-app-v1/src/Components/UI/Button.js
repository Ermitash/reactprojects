import styles from './Button.module.css';

function Button({ type }) {
  return (
    <button className={styles.Button} type={type}>
      Submit
    </button>
  );
}

export default Button;
