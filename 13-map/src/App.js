import { useState } from 'react';
import './App.css';
import Button from './Components/Button';
import Counter from './Components/Counter';

const texts = ['Biba', 'Ulala', 'Wow', 'Click me!', 'Gopa', 'Tupa poel govna!'];

function App() {
  const [count, setCount] = useState(0);
  const incrementCount = () => {
    setCount(count + 1);
  };

  return (
    <div className="App">
      <Counter count={count} />
      {texts.map((text, number) => {
        return <Button text={text} onClick={incrementCount} key={number} />;
      })}
    </div>
  );
}

export default App;
