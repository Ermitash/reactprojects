import React from 'react';
import { Link, useParams } from 'react-router-dom';

const SingleCourse = ({ biba }) => {
  const params = useParams();
  console.log(params);
  console.log(biba);
  return (
    <>
      <h1>{params.slug}</h1>
      <Link to="../">Return to Courses</Link>
    </>
  );
};

export default SingleCourse;
