import { Link, Outlet, Route, Routes } from 'react-router-dom';
import courses from '../data/courses';

const Courses = () => {
  return (
    <>
      <h1>Courses</h1>
      {courses.map((value) => {
        return (
          <div key={value.id}>
            <Link to={value.slug}>{value.title}</Link>
          </div>
        );
      })}
      <Outlet />
    </>
  );
};

export default Courses;
