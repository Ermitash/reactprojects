import './App.css';
import Wrapper from './Components/Wrapper';

function App() {
  return (
    <div className="App">
      <Wrapper color="lightblue">
        <h2>Text inside of the Wrapper</h2>
        <button>Click me!</button>
      </Wrapper>
      <Wrapper color="lightgreen">
        <h2>Another text</h2>
        <p>Some description</p>
        <input type="Text" placeholder="Enter value" />
      </Wrapper>
      <Wrapper color="orange">
        <h3>Login:</h3>
        <input type="Text" placeholder="Enter login" />
        <h3>Password:</h3>
        <input type="password" placeholder="Enter password" />
      </Wrapper>
    </div>
  );
}

export default App;
