import TodoForm from './Components/Todos/TodoForm';
import TodoControlPanel from './Components/Todos/TodoControlPanel';
import { TodoList } from './Components/Todos/TodoList';
import './App.css';

function App() {
  return (
    <div className="App">
      <h1>Todo App</h1>
      <TodoForm />
      <TodoControlPanel />
      <TodoList />
    </div>
  );
}

export default App;
