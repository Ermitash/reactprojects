import styles from './TodoList.module.css';
import Todo from './Todo';
import { useState } from 'react';

var setTodoList_External = null;
var getTodoList_External = null;
var removeElementFromTodoList_External = null;
var removeCheckedElementsFromTodoList_External = null;

var globalTodoListKey = 0;

function IncrementGlobalTodoListKey() {
  globalTodoListKey++;
}

function TodoList() {
  function removeElementFromTodoList(index) {
    setTodoList((oldArray) => {
      oldArray.splice(index, 1);
      return [...oldArray];
    });
  }

  function removeCheckedElementsFromTodoList() {
    setTodoList(
      todoList.filter((value) => {
        return !value[2];
      })
    );
  }

  const [todoList, setTodoList] = useState([]);

  getTodoList_External = () => {
    return todoList;
  };
  setTodoList_External = setTodoList;
  removeElementFromTodoList_External = removeElementFromTodoList;
  removeCheckedElementsFromTodoList_External =
    removeCheckedElementsFromTodoList;

  return todoList.length === 0 ? (
    <h1>Empty todo!</h1>
  ) : (
    <div className={styles.TodoList}>
      {todoList.map((value, index) => {
        return <Todo key={value[0]} id={index} text={value[1]} />;
      })}
    </div>
  );
}

export {
  setTodoList_External,
  getTodoList_External,
  removeElementFromTodoList_External,
  removeCheckedElementsFromTodoList_External,
  globalTodoListKey,
  IncrementGlobalTodoListKey,
  TodoList,
};
