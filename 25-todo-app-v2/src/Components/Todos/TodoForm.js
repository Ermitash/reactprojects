import React, { useState } from 'react';
import { SubmitButton } from '../UI/Buttons';
import {
  setTodoList_External,
  globalTodoListKey,
  IncrementGlobalTodoListKey,
} from './TodoList';
import styles from './TodoForm.module.css';

function TodoForm() {
  const [inputData, setInputData] = useState('');

  const onFromSubmit = (event) => {
    event.preventDefault();

    if (inputData != '') {
      setTodoList_External((oldArray) => [
        ...oldArray,
        [globalTodoListKey, inputData, false],
      ]);
      IncrementGlobalTodoListKey();
      setInputData('');
    }
  };

  return (
    <form className={styles.TodoForm} onSubmit={onFromSubmit}>
      <input
        className={styles.TodoForm_input}
        type="text"
        value={inputData}
        onChange={(e) => {
          setInputData(e.target.value);
        }}
        placeholder="Enter new todo"
      />
      <SubmitButton />
    </form>
  );
}

export default TodoForm;
