import {
  DeleteAllTodosButton,
  DeleteCompletedTodosButton,
} from '../UI/Buttons';
import {
  removeCheckedElementsFromTodoList_External,
  setTodoList_External,
} from './TodoList';
import styles from './TodoControlPanel.module.css';

function TodoControlPanel() {
  return (
    <div className={styles.TodoControlPanel}>
      <DeleteAllTodosButton
        onClickFunc={() => {
          setTodoList_External([]);
        }}
      />
      <DeleteCompletedTodosButton
        onClickFunc={() => {
          removeCheckedElementsFromTodoList_External();
        }}
      />
    </div>
  );
}

export default TodoControlPanel;
