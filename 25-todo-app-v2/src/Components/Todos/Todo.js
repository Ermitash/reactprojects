import {
  removeElementFromTodoList_External,
  setTodoList_External,
  getTodoList_External,
} from './TodoList';
import { DeleteButton, CheckButton } from '../UI/Buttons';
import journalIcon from '../../Img/journal-text.png';
import styles from './Todo.module.css';
import { useState } from 'react';

function Todo({ text, id }) {
  function removeElementFromTodoList() {
    removeElementFromTodoList_External(id);
  }

  function onCheckButtonClick() {
    setToggleChecked(!bToggleChecked);

    setTodoList_External((oldArray) => {
      oldArray[id][2] = !bToggleChecked;
      return [...oldArray];
    });
  }

  const [bToggleChecked, setToggleChecked] = useState(false);

  let currentTodoStyle = null;
  let currentJournalIconStyle = null;
  let currentTextStyle = null;

  if (bToggleChecked) {
    currentTodoStyle = styles.checkedTodo;
    currentJournalIconStyle = styles.checkedJournalIcon;
    currentTextStyle = styles.CheckedText;
  } else {
    currentTodoStyle = styles.Todo;
    currentJournalIconStyle = styles.journalIcon;
    currentTextStyle = styles.Text;
  }

  return (
    <div className={currentTodoStyle}>
      <div className={styles.LeftTodoContainer}>
        <img className={currentJournalIconStyle} src={journalIcon} alt="icon" />
        <h2 className={currentTextStyle}>{text}</h2>
      </div>
      <div className={styles.RightTodoContainer}>
        <DeleteButton
          onClickFunc={removeElementFromTodoList}
          bToggleChecked={bToggleChecked}
        />
        <CheckButton
          onClickFunc={onCheckButtonClick}
          bToggleChecked={bToggleChecked}
        />
      </div>
    </div>
  );
}

export default Todo;
