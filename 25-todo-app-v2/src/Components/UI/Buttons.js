import { useEffect, useState } from 'react';
import { getTodoList_External } from '../Todos/TodoList';
import styles from './Buttons.module.css';
import { BiCheck } from 'react-icons/bi';
import { RiDeleteBin2Line, RiRefreshLine } from 'react-icons/ri';

function SubmitButton() {
  return (
    <button className={styles.SubmitButton} type="submit">
      Submit
    </button>
  );
}

const IconPropertyData = {
  size: 30,
  color: 'rgba(177, 177, 177, 0.5)',
  style: styles.Icon,
};

const IconData = {
  Icon: RiDeleteBin2Line,
  standart: IconPropertyData,
  hovered: IconPropertyData,
};

function ButtonWithIcon({ Data, onClickFunc, buttonStyle, bDisabled }) {
  const [mouseEntered, setMouseEntered] = useState(false);

  let currentIconPropertyData = null;

  if (Data) {
    currentIconPropertyData = mouseEntered ? Data.hovered : Data.standart;
  }

  const setCurrentIconPropertyData = (InData) => {
    currentIconPropertyData = InData;
  };

  return (
    <button
      className={buttonStyle}
      disabled={bDisabled}
      onClick={onClickFunc}
      onMouseEnter={() => {
        if (Data) {
          setCurrentIconPropertyData(Data.hovered);
        }
        setMouseEntered(true);
      }}
      onMouseLeave={() => {
        if (Data) {
          setCurrentIconPropertyData(Data.standart);
        }
        setMouseEntered(false);
      }}
    >
      {Data && (
        <Data.Icon
          className={currentIconPropertyData.style}
          size={currentIconPropertyData.size}
          color={currentIconPropertyData.color}
        />
      )}
    </button>
  );
}

function DeleteButton({ onClickFunc, bToggleChecked }) {
  const uncheckedIconData = {
    ...IconData,
    Icon: RiDeleteBin2Line,
    hovered: { ...IconData.hovered, color: 'red' },
  };

  const checkedIconData = {
    ...IconData,
    Icon: RiDeleteBin2Line,
  };

  let currentIconData = null;
  let currentButtonStyle = null;

  if (bToggleChecked) {
    currentIconData = checkedIconData;
    currentButtonStyle = styles.ButtonWithIcon;
  } else {
    currentIconData = uncheckedIconData;
    currentButtonStyle = styles.ButtonWithIcon;
  }

  return (
    <ButtonWithIcon
      Data={currentIconData}
      onClickFunc={onClickFunc}
      buttonStyle={currentButtonStyle}
    />
  );
}

function CheckButton({ onClickFunc, bToggleChecked }) {
  const uncheckedIconData = {
    ...IconData,
    Icon: BiCheck,
    standart: { ...IconData.standart, size: 40 },
    hovered: { ...IconData.hovered, size: 40, color: 'green' },
  };

  const checkedIconData = {
    ...IconData,
    Icon: BiCheck,
    standart: {
      ...IconData.standart,
      size: 40,
    },
    hovered: {
      ...IconData.hovered,
      size: 40,
    },
  };

  let currentIconData = null;
  let currentButtonStyle = styles.ButtonWithIcon;

  if (bToggleChecked) {
    currentIconData = checkedIconData;
  } else {
    currentIconData = uncheckedIconData;
  }

  return (
    <ButtonWithIcon
      Data={currentIconData}
      onClickFunc={onClickFunc}
      buttonStyle={currentButtonStyle}
    />
  );
}

function DeleteCompletedTodosButton({ onClickFunc }) {
  const currentIconData = {
    ...IconData,
    standart: { ...IconData.standart, color: 'black' },
    hovered: { ...IconData.hovered, color: 'black' },
  };

  return (
    <ButtonWithIcon
      Data={currentIconData}
      buttonStyle={styles.DeleteCompletedTodosButton}
      onClickFunc={onClickFunc}
    />
  );
}

function DeleteAllTodosButton({ onClickFunc }) {
  const currentIconData = {
    ...IconData,
    Icon: RiRefreshLine,
    standart: { ...IconData.standart, color: 'black' },
    hovered: { ...IconData.hovered, color: 'black' },
  };

  return (
    <ButtonWithIcon
      Data={currentIconData}
      buttonStyle={styles.DeleteAllTodosButton}
      onClickFunc={onClickFunc}
    />
  );
}

export {
  SubmitButton,
  DeleteButton,
  CheckButton,
  DeleteCompletedTodosButton,
  DeleteAllTodosButton,
};
